﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pruffer
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strings = Console.ReadLine().Split(' ');//считываем код прюффера
            IList<int> arr = new List<int>();//массив, в котором храниться код прюффера
            foreach (var s in strings)
            {
                arr.Add(int.Parse(s));
            }
            List<int> B = new List<int>();//массив B, который заполнен числами с 1 до размера кода прюфера + 2
            for (int i = 1; i < arr.Count + 3; i++)
            {
                B.Add(i);
            }
            int n = arr.Count + 2;
            for (int i = 0; i < arr.Count; i++)
            {
                int minimum = n;
                foreach (int b in B)
                {
                    if (!arr.Contains(b))
                    {
                        minimum = Math.Min(minimum, b);
                    }
                }
                Console.WriteLine(arr[i].ToString() + " " + minimum.ToString());
                arr.RemoveAt(i);
                i--;
                B.Remove(minimum);
            }
            Console.WriteLine(B.First().ToString() + " " + B.Last());

        }
    }
}